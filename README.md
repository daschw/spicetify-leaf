# spicetify-leaf

A [spicetify](https://github.com/spicetify/spicetify-cli) theme using the colors from the awesome [KDE Leaf Plasma Theme](https://github.com/qewer33/leaf-kde).

![spicetify leaf](assets/screenshot.png)

## Installation

Make sure to have [spicetify](https://github.com/spicetify/spicetify-cli) installed.

```bash
git clone https://codeberg.org/daschw/spicetify-leaf
cd spicetify-leaf
ln -s leaf/ ~/.config/spicetify/Themes/
```

## Usage

```bash
spicetify config current_theme leaf
spicetify config color_cheme dark # or light
spicetify apply
```
